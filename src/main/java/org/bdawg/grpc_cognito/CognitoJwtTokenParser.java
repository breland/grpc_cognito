package org.bdawg.grpc_cognito;

import com.avast.grpc.jwt.server.JwtTokenParser;
import com.google.common.base.Strings;
import org.keycloak.TokenVerifier;
import org.keycloak.common.VerificationException;
import org.keycloak.representations.AccessToken;

import java.security.PublicKey;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionException;

public class CognitoJwtTokenParser implements JwtTokenParser<AccessToken> {

    private final CognitoPublicKeyProvider keyProvider;
    protected String expectedAudience;
    protected String expectedIssuedFor;
    protected final TokenVerifier.Predicate<AccessToken>[] checks;

    public CognitoJwtTokenParser(CognitoPublicKeyProvider keyProvider, CognitoJwtConfiguration config) {
        this.keyProvider = keyProvider;
        this.checks =
                new TokenVerifier.Predicate[] {
                        // new TokenVerifier.RealmUrlCheck(realmUrl),
                        TokenVerifier.SUBJECT_EXISTS_CHECK,
                        // new TokenVerifier.TokenTypeCheck(TokenUtil.TOKEN_TYPE_BEARER),
                        TokenVerifier.IS_ACTIVE,
                        new IssuerCheck(config.getExpectedIssuer()),
                };
    }

    @Override
    public CompletableFuture<AccessToken> parseToValid(String jwtToken) {
        TokenVerifier<AccessToken> verifier;
        try {
            verifier = createTokenVerifier(jwtToken);
        } catch (VerificationException e) {
            CompletableFuture<AccessToken> r = new CompletableFuture<>();
            r.completeExceptionally(e);
            return r;
        }
        return CompletableFuture.supplyAsync(
                () -> {
                    try {
                        return verifier.verify().getToken();
                    } catch (VerificationException e) {
                        throw new CompletionException(e);
                    }
                });
    }

    protected TokenVerifier<AccessToken> createTokenVerifier(String jwtToken)
            throws VerificationException {
        TokenVerifier<AccessToken> verifier =
                TokenVerifier.create(jwtToken, AccessToken.class).withChecks(checks);
        if (!Strings.isNullOrEmpty(expectedAudience)) {
            verifier = verifier.audience(expectedAudience);
        }
        if (!Strings.isNullOrEmpty(expectedIssuedFor)) {
            verifier = verifier.issuedFor(expectedIssuedFor);
        }

        try {
            PublicKey pk = this.keyProvider.get(verifier.getHeader().getKeyId());
            verifier.publicKey(pk);
        } catch (CognitoPublicKeyProvider.PublicKeyException ex) {
            throw new VerificationException();
        }
        return verifier;
    }


    public CognitoJwtTokenParser withExpectedAudience(String expectedAudience) {
        this.expectedAudience = expectedAudience;
        return this;
    }

    public CognitoJwtTokenParser withExpectedIssuedFor(String expectedIssuedFor) {
        this.expectedIssuedFor = expectedIssuedFor;
        return this;
    }

}
