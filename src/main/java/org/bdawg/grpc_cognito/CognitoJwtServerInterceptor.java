package org.bdawg.grpc_cognito;

import com.avast.grpc.jwt.server.JwtServerInterceptor;
import com.avast.grpc.jwt.server.JwtTokenParser;
import io.grpc.*;
import org.keycloak.representations.AccessToken;

import java.util.Arrays;
import java.util.List;


public class CognitoJwtServerInterceptor extends JwtServerInterceptor<AccessToken> {

    private static final List<String> PUBLIC_METHOD_NAMES = Arrays.asList("");

    public CognitoJwtServerInterceptor(JwtTokenParser<AccessToken> tokenParser) {
        super(tokenParser);
    }

    public static CognitoJwtServerInterceptor fromConfig(CognitoJwtConfiguration config) {
        CognitoPublicKeyProvider cognitoPublicKeyProvider = new CognitoDownloadingPublicKeyProvider(config.getAwsRegion(), config.getAwsSuffix(), config.getUserPoolId(), config.getClock(), config.getMinTimeBetweenJwksRequests(), config.getPublicKeyCacheTtl());
        CognitoJwtTokenParser parser = new CognitoJwtTokenParser(cognitoPublicKeyProvider, config);
        parser = parser.withExpectedAudience(config.getAudience());
        return new CognitoJwtServerInterceptor(parser);
    }

    public <ReqT, RespT> ServerCall.Listener<ReqT> interceptCall(
            ServerCall<ReqT, RespT> call, Metadata headers, ServerCallHandler<ReqT, RespT> next) {
        if (PUBLIC_METHOD_NAMES.contains(call.getMethodDescriptor().getFullMethodName())) {
            // just do it
            return next.startCall(call, headers);
        } else {
            return super.interceptCall(call, headers, next);
        }
    }
}
