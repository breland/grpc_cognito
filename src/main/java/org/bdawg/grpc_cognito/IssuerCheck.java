package org.bdawg.grpc_cognito;

import org.keycloak.TokenVerifier;
import org.keycloak.common.VerificationException;
import org.keycloak.representations.JsonWebToken;

public class IssuerCheck implements TokenVerifier.Predicate<JsonWebToken> {
    private final String expectedIssuer;

    public IssuerCheck(String expectedIssuer) {
        this.expectedIssuer = expectedIssuer;
    }

    @Override
    public boolean test(JsonWebToken t) throws VerificationException {
        if (expectedIssuer == null) {
            throw new VerificationException("Missing expectedIssuer");
        }

        String issuer = t.getIssuer();
        if (issuer == null) {
            throw new VerificationException("No issuer in the token");
        }

        if (expectedIssuer.equals(issuer)) {
            return true;
        }

        throw new VerificationException("Expected issuer did not match");
    }
}
