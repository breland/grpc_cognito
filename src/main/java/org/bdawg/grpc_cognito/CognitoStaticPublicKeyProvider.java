package org.bdawg.grpc_cognito;

import java.security.PublicKey;
import java.util.HashMap;
import java.util.Map;

public class CognitoStaticPublicKeyProvider implements CognitoPublicKeyProvider {
    private final Map<String, PublicKey> keys;

    public CognitoStaticPublicKeyProvider(Map<String, PublicKey> keys) {
        this.keys = new HashMap<>();
        this.keys.putAll(keys);
    }

    @Override
    public PublicKey get(String keyID) {
        return this.get(keyID);
    }
}
