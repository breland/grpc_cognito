package org.bdawg.grpc_cognito;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.keycloak.jose.jwk.JSONWebKeySet;
import org.keycloak.jose.jwk.JWK;
import org.keycloak.util.JWKSUtils;

import java.io.IOException;
import java.net.URL;
import java.security.PublicKey;
import java.time.Clock;
import java.time.Duration;
import java.time.Instant;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class CognitoDownloadingPublicKeyProvider implements CognitoPublicKeyProvider {

    private static final String JWKS_LOCATION_FORMAT = "https://cognito-idp.%s%s/%s/.well-known/jwks.json";

    private final String jwksLocation;
    private final Map<String, PublicKey> currentKeys;
    private final ObjectMapper objectMapper;
    private volatile Instant lastRequestTime = Instant.MIN;
    private final Clock clock;
    private final Duration minTimeBetweenJwksRequests;
    private final Duration publicKeyCacheTtl;

    public CognitoDownloadingPublicKeyProvider(String region, String suffix, String userPoolId, Clock clock, Duration minTimeBetweenJwksRequests, Duration publicKeyCacheTtl) {
        this.jwksLocation = String.format(JWKS_LOCATION_FORMAT, region, suffix, userPoolId);
        this.currentKeys = new ConcurrentHashMap<>();
        this.objectMapper = new ObjectMapper();
        this.clock = clock;
        this.minTimeBetweenJwksRequests = minTimeBetweenJwksRequests;
        this.publicKeyCacheTtl = publicKeyCacheTtl;
    }

    @Override
    public PublicKey get(String keyId) throws PublicKeyException {
        if (lastRequestTime.plus(publicKeyCacheTtl).isBefore(clock.instant())) {
            updateKeys();
        }
        PublicKey fromCache = currentKeys.get(keyId);
        if (fromCache != null) {
            return fromCache;
        }
        updateKeys();
        PublicKey res = currentKeys.get(keyId);
        if (res == null) {
            throw new KeyNotFoundException("Key with following ID not found: " + keyId);
        }
        return res;
    }

    protected void updateKeys() throws PublicKeyException {
        synchronized (this) {
            if (clock.instant().isAfter(lastRequestTime.plus(minTimeBetweenJwksRequests))) {
                Map<String, PublicKey> newKeys = fetchNewKeys();
                currentKeys.clear();
                currentKeys.putAll(newKeys);
                lastRequestTime = clock.instant();
            }
        }
    }

    protected Map<String, PublicKey> fetchNewKeys() throws PublicKeyException {
        try {
            JSONWebKeySet jwks = this.objectMapper.readValue(new URL(this.jwksLocation).openStream(), JSONWebKeySet.class);
            return JWKSUtils.getKeysForUse(jwks, JWK.Use.SIG);
        } catch (IOException ex) {
            throw new PublicKeyException(ex);
        }
    }


}
