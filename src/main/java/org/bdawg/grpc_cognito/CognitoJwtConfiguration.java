package org.bdawg.grpc_cognito;


import java.time.Clock;
import java.time.Duration;

public interface CognitoJwtConfiguration {

    default String getAwsPartition() {
        return "aws";
    }

    default String getAwsSuffix() {
        if (getAwsPartition().equalsIgnoreCase("aws")) {
            return ".amazonaws.com";
        }
        throw new IllegalArgumentException();
    }

    String getAwsRegion();

    default String getExpectedIssuer() {
        return String.format("https://cognito-idp.%s%s/%s", getAwsRegion(), getAwsSuffix(), getUserPoolId());
    }

    default String getUserPoolId() {
        return System.getenv().get("USER_POOL_ID");
    }

    default String getAudience() {
        return System.getenv("AUDIENCE");
    }

    default Clock getClock() {
        return Clock.systemUTC();
    }

    default Duration getMinTimeBetweenJwksRequests(){
        return Duration.ofDays(1);
    }

    default Duration getPublicKeyCacheTtl() {
        return Duration.ofDays(7);
    }

}
