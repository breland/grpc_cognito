package org.bdawg.grpc_cognito;

import java.security.PublicKey;

public interface CognitoPublicKeyProvider {

    class PublicKeyException extends Exception {
        public PublicKeyException(String message) {
            super(message);
        }

        public PublicKeyException(Exception root) {
            super(root);
        }
    }

    class KeyNotFoundException extends PublicKeyException {
        public KeyNotFoundException(String message) {
            super(message);
        }
    }

    PublicKey get(String keyID) throws PublicKeyException;
}
