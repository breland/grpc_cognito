package org.bdawg.grpc_cognito;

import org.junit.Test;
import org.keycloak.common.VerificationException;
import org.keycloak.representations.JsonWebToken;
import org.mockito.Mockito;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

public class IssuerCheckTest {



    @Test(expected = VerificationException.class)
    public void testThrowsOnNoMatch() throws VerificationException {
        IssuerCheck issuerCheck = new IssuerCheck("hello");
        JsonWebToken token = Mockito.mock(JsonWebToken.class);
        Mockito.when(token.getIssuer()).thenReturn("goTeam");
        issuerCheck.test(token);
        fail("Should not have gotten here");
    }


    @Test
    public void testAccpetsCorrectToken() throws VerificationException {
        IssuerCheck issuerCheck = new IssuerCheck("goTeam");
        JsonWebToken token = Mockito.mock(JsonWebToken.class);
        Mockito.when(token.getIssuer()).thenReturn("goTeam");
        assertTrue(issuerCheck.test(token));
    }
}
