# GRPC Cognito


A small utility library for using Cognito JWTs with GRPC

## Usage

```java

int port = 8080;
// Feel free to build actual concrete implementations of this - it's just an interface with defaults
CognitoJwtConfiguration config = new CognitoJwtConfiguration() {
    public String getAwsRegion() {
       return "us-west-2";
    }
    public String getUserPoolId() {
        return "YourUserPoolId";
    }
    
    public String getAudience() {
        return "ThisAudeince";
    }
};
ServerBuilder serverBuilder = ServerBuilder.forPort(port);
CognitoJwtServerInterceptor cognitoJwtServerInterceptor = CognitoJwtServerInterceptor.fromConfig(config);
ServiceImpl myServiceImpl; = new MyServiceImpl(cognitoJwtServerInterceptor.AccessTokenContextKey);
serverBuilder.addService(ServerInterceptors.intercept(myServiceImpl, cognitoJwtServerInterceptor));
Server server = serverBuilder.build().start();

```

```java

public class ServiceImpl extends ServiceGrpc.ServiceImplBase {

  private final Context.Key<AccessToken> accessTokenKey;

  public ServiceImpl(Context.Key<AccessToken> accessTokenKey) {
          if (accessTokenKey == null) {
              throw new RuntimeException("Access token key is required");
          }
          this.accessTokenKey = accessTokenKey;
  }



    @Override
    public void myMethod(ServiceOuterClass.MyMethodRequest request, StreamObserver<ServiceOuterClass.MyMethodResult> responseObserver) {
        Object subject = this.accessTokenKey.get().getSubject();
        ...
    }

}

```
