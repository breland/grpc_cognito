# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [1.2.2](https://gitlab.com/breland/grpc_cognito/compare/v1.2.1...v1.2.2) (2020-08-01)


### Bug Fixes

* ensure we bump version.txt ([1ed2d62](https://gitlab.com/breland/grpc_cognito/commit/1ed2d621d40182502c6e94001be1879bdb7ba9a3))

### [1.2.1](https://gitlab.com/breland/grpc_cognito/compare/v1.1.1...v1.2.1) (2020-08-01)


### Bug Fixes

* fix version reading ([d48b3b7](https://gitlab.com/breland/grpc_cognito/commit/d48b3b79a9a8e5644e65cedbed677355de55352f))
* job fixes ([cbb36af](https://gitlab.com/breland/grpc_cognito/commit/cbb36afefffa239fb495f0c9da36209412b064cd))
* tag ([cdba54f](https://gitlab.com/breland/grpc_cognito/commit/cdba54f5c00d0595dc9d1b0f8f7027594ad766c6))
* tag ([343b762](https://gitlab.com/breland/grpc_cognito/commit/343b7623688e8f07c50761cac5d495bdef2bb221))
* tags ([d1fda33](https://gitlab.com/breland/grpc_cognito/commit/d1fda33d1b76a68f400b819f7befb6d2b35b7c07))

### [1.1.1](https://gitlab.com/breland/grpc_cognito/compare/v1.1.0...v1.1.1) (2020-08-01)


### Bug Fixes

* fix version strategy ([8b3a5da](https://gitlab.com/breland/grpc_cognito/commit/8b3a5da03e9767b45322a9bd9f49c04152e113ef))

## 1.1.0 (2020-08-01)


### Features

* add auto version ([4723a8e](https://gitlab.com/breland/grpc_cognito/commit/4723a8ec31fb223dbccc8a2e9c1932e9f961d73b))


### Bug Fixes

* fix publishing ([e0a99a5](https://gitlab.com/breland/grpc_cognito/commit/e0a99a59a06a67ed80fa0209ccfb4a5b176a9c19))
* fix version ([2de6dd4](https://gitlab.com/breland/grpc_cognito/commit/2de6dd4ecb06b57a34f0dee8cc0a5404f90f6cba))
* origin when pushing ([824320d](https://gitlab.com/breland/grpc_cognito/commit/824320df65504b1c024547238592118b8e50e84a))
* publish origin fix ([aa726d4](https://gitlab.com/breland/grpc_cognito/commit/aa726d4e45a42184a4c448acf3f976c0a7dea5aa))
* update push command ([e0f61e9](https://gitlab.com/breland/grpc_cognito/commit/e0f61e9988c565ad1735bc7c1c2f11e87fc62bcc))

## [1.1.0](https://gitlab.com/breland/grpc_cognito/compare/v1.0.0...v1.1.0) (2020-08-01)


### Features

* add auto version ([4723a8e](https://gitlab.com/breland/grpc_cognito/commit/4723a8ec31fb223dbccc8a2e9c1932e9f961d73b))

## 1.0.0 (2020-08-01)
